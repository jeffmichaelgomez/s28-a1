const url = 'https://jsonplaceholder.typicode.com/todos';

//3 & 4
fetch(url).then((res) => res.json())
.then((json) => console.log(json.map(element => element.title)));

//5
fetch(`${url}/1`).then((res) => res.json())
.then((json) => console.log(json));

//6
fetch(`${url}/1`).then((res) => res.json())
.then((json) => console.log(`The item "${json.title}" on the list has a status of ${json.completed}.`));

//7
fetch(url,{
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: 'Created To Do List Item',
		userId: 1
	})
}).then((res) => res.json())
.then((json) => console.log(json));


//8 & 9
fetch(`${url}/1`,{
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structure",
		id: 1,
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
}).then((res) => res.json())
.then((json) => console.log(json));

//10 & 11
fetch(`${url}/1`,{
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "07/09/21",
		status: "Complete",
	})
}).then((res) => res.json())
.then((json) => console.log(json));

//12
fetch(`${url}/1`, { method: 'DELETE' });
